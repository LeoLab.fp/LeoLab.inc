unit ull_hashrec;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,
  uits_errors;

type

  TLL_HashRecItemFree=procedure(p:Pointer);

  PLL_HashRecItem=^TLL_HashRecItem;
  TLL_HashRecItem=record
    id:byte;
    next:array of PLL_HashRecItem;
    data:Pointer;
  end;

  TLL_HashRec=class(TObject)
  private
    Items:PLL_HashRecItem;
  protected
    function _find(h:string;hIdx:integer=0;Lst:PLL_HashRecItem=nil):PLL_HashRecItem;
  public
    function Find(h:string):Pointer;

    constructor Create();
    destructor Destroy();override;
  end;


implementation

function TLL_HashRec._find(h:string;hIdx:integer=0;Lst:PLL_HashRecItem=nil):PLL_HashRecItem;
var
  idx:byte;
  i:integer;
  PR:PLL_HashRecItem;
begin
  Result:=nil;
  if Length(h)<hIdx then raise EITS_OutOfRange.Create('Index lager then string.');
  if idx=0 then idx:=1;
  if Lst=nil then Lst:=Self.Items;
  idx:=ord(h[hIdx]);
  PR:=nil;
  for i:=0 to Length(Lst^.next) do
  begin
    if Lst^.next[i]^.id=idx then
    begin
      PR:=Lst^.next[i];
      break;
    end;
  end;
  if PR<>nil then
  begin
    if Length(h)>hIdx then Result:=Self._find(h,hIdx+1,PR);
  end;
end;

function TLL_HashRec.Find(h:string):Pointer;
var
  PR:PLL_HashRecItem;
begin
  Result:=nil;
  PR:=Self._find(h);
  if PR<>nil then Result:=PR^.data;
end;

constructor TLL_HashRec.Create();
begin
  inherited Create();
  //
end;

destructor TLL_HashRec.Destroy();
begin
  //
  inherited Destroy();
end;

end.

