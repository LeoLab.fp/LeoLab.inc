unit ull_fileUtils;
{ $DEFINE dbg}
(*
  Модуль содержит расширенные утилиты для работы с файлами и папками
*)

{$mode objfpc}{$H+}

interface

uses
  SysUtils,BaseUnix,Dos,syslog,ull_strutils;

type
  TLL_FileError=(
    feNoError,          // Нет ошибок
    feEmpty,            // Пустой аргумент
    feCreateError,      // Ошибка создания
    feDeleteError,      // Ошибка удаления
    feFileExists,       // Файл существует
    feNotFound,         // Не найдено
    feError             // Общая ошибка
  );

var
  lastErr:TLL_FileError;

//type

  function ll_RealPath(const APath:string;const exePath:boolean=false):string; //Получить реальный, абсолютный путь.
  function ll_RealFileName(const AFile:string;const exePath:boolean=false):string; //Получить абсолютный путь к файлу.
  function ll_MKPath(const APath:string):boolean; //Создать папку и полный путь к ней.
  function ll_RMPath(const APath:string):boolean; //Удалить папку и все ее содержимое.
  function ll_upDir(const APath:string):string; //Вернуть родительскую папку.
  function ll_checkPath(const APath:string;const create:boolean=false):boolean; //Проверить наличие пути, создать при create=true

  function ll_FindExe(const AFile:string; const usePath:boolean=true):string; //Найти исполняемый файл

implementation

function __isDir(const AName:string):boolean;
begin
  {$ifdef dbg}syslog.dbgout('__isDir','Enter('+AName+')');{$endif}
  Result:=FileExists(AName)and((FileGetAttr(AName)and faDirectory)<>0);
  {$ifdef dbg}syslog.dbgout('__isDir','Leave('+BoolToStr(Result,true)+')');{$endif}
end;

function __canExec(const AName:string):boolean;
begin
  {$ifdef dbg}syslog.dbgout('__canExec','Enter('+AName+')');{$endif}
  Result:=((fpAccess(AName,X_OK)=0)and(not __isDir(AName)));
  {$ifdef dbg}syslog.dbgout('__canExec','Leave('+BoolToStr(Result,true)+')');{$endif}
end;

function ll_checkPath(const APath:string;const create:boolean=false):boolean;
begin
  {$ifdef dbg}syslog.dbgout('ll_checkPath','Enter('+APath+',create:'+BoolToStr(create,true)+')');{$endif}
  Result:=__isDir(APath);
  if (not Result) and create then Result:=ll_mkPath(APath);
  {$ifdef dbg}syslog.dbgout('ll_checkPath','Leave('+BoolToStr(Result,true)+')');{$endif}
end;

function ll_upDir(const APath:string):string;
begin
  {$ifdef dbg}syslog.dbgout('ll_upDir','Enter('+APath+')');{$endif}
  Result:=Copy(APath,1,LastDelimiter(DirectorySeparator,APath)-1);
  {$ifdef dbg}syslog.dbgout('ll_upDir','Leave('+Result+')');{$endif}
end;

function ll_RealFileName(const AFile:string; const exePath:boolean=false):string;
begin
  {$ifdef dbg}syslog.dbgout('ll_RealFileName','Enter('+AFile+',exePath:'+BoolToStr(exePath,true)+')');{$endif}
  if AFile<>'' then Result:=ll_RealPath(ExtractFilePath(AFile),exePath)+DirectorySeparator+ExtractFileName(AFile) else Result:='';
  {$ifdef dbg}syslog.dbgout('ll_RealFileName','Leave('+Result+')');{$endif}
end;

function ll_FindExe(const AFile:string; const usePath:boolean=true):string;
var
  _fp,_fn:string;
  _pa:TStringArray;
  c:integer;
begin
  {$ifdef dbg}syslog.dbgout('ll_FindExe','Enter('+AFile+')');{$endif}
  lastErr:=feNoError;
  try
    Result:=AFile;
    _fp:=ExtractFilePath(AFile);
    _fn:=ExtractFileName(AFile);
    if (_fp='') or (_fp='./') then
    begin
      Result:=GetCurrentDir()+'/'+_fn;
      if __canExec(Result) then exit();
      Result:=ExtractFilePath(paramstr(0))+_fn;
      if __canExec(Result) then exit();
      if ((_fp='') and usePath) then
      begin
        _pa:=Explode(':',GetEnv('PATH'));
        for c:=0 to Length(_pa)-1 do
        begin
          Result:=_pa[c]+'/'+_fn;
          if __canExec(Result) then exit();
        end;
      end;
      Result:=AFile;
    end else if __canExec(AFile) then Result:=AFile else Result:='';
  finally
    {$ifdef dbg}syslog.dbgout('ll_FindExe','Leave('+Result+')');{$endif}
  end;
end;

(*
APath can start:
      ./ => GetCurrentDir()+AFile[1..] || ExtractFilePath(paramstr(0))+AFile[1..]
      ../ => __getUpDir(GetCurrentDir())+AFile[2..] || __getUpDir(ExtractFilePath(paramstr(0))+AFile[2..]
*)

function ll_RealPath(const APath:string;const exePath:boolean=false):string;
var
  sa:TStringArray;
  bp:string;
  c:integer;

  function __exePath():string;
  begin
    Result:=ExtractFilePath(ParamStr(0));
    if Result[Length(Result)]=DirectorySeparator then Result:=Copy(Result,1,Length(Result)-1);
  end;

  function __curPath():string;
  begin
    Result:=GetCurrentDir();
    if Result[Length(Result)]=DirectorySeparator then Result:=Copy(Result,1,Length(Result)-1);
  end;

  function __getDir(const exePath:boolean):string;
  begin
    if exePath then Result:=__exePath() else Result:=__curPath();
  end;

begin
  {$ifdef dbg}syslog.dbgout('ll_RealPath','Enter('+APath+')');{$endif}
  try

    sa:=Explode(DirectorySeparator,APath);
//    if sa=nil then exit(APath+DirectorySeparator);
    if sa=nil then exit('');
    if sa[0]='.' then
    begin
      sa[0]:=__getDir(exePath);
      Result:=Implode(DirectorySeparator,sa,true);
    end else if sa[0]='~' then
    begin
      sa[0]:=ExtractFilePath(GetEnv('HOME'));
      Result:=Implode(DirectorySeparator,sa,true);
    end else if sa[0]='..' then
    begin
      bp:=__getDir(exePath);
      for c:=0 to Length(sa)-1 do
      begin
        if sa[c]='' then continue;
        if sa[c]='..' then bp:=ll_upDir(bp) else bp:=bp+DirectorySeparator+sa[c];
      end;
      Result:=bp;
    end else Result:=APath;
  finally
    {$ifdef dbg}syslog.dbgout('ll_RealPath','Leave('+Result+')');{$endif}
  end;
end;

function ll_MKPath(const APath:string):boolean;
var
  p:string;
begin
  {$ifdef dbg}syslog.dbgout('ll_MkPath','Enter('+APath+')');{$endif}
  lastErr:=feNoError;Result:=true;
  p:=ll_RealPath(APath);
  try
    if p='' then begin lastErr:=feEmpty;exit(false); end;
    if FileExists(p) then begin exit(__isDir(p));end;
    if APath[Length(p)]='/' then p:=copy(p,0,Length(p)-1);
    if not FileExists(Copy(p,1,LastDelimiter('/',p))) then ll_MKPath(Copy(p,1,LastDelimiter('/',p)));
    if not CreateDir(p) then begin lastErr:=feCreateError;Result:=false; end;
  finally
    {$ifdef dbg}syslog.dbgout('ll_MkPath','Leave('+BoolToStr(Result,true)+')');{$endif}
  end;
end;

function ll_RMPath(const APath:string):boolean;
begin
  {$ifdef dbg}syslog.dbgout('ll_RMPath','Enter('+APath+')');{$endif}
  Result:=false;
  {$ifdef dbg}syslog.dbgout('ll_RMPath','Leave('+BoolToStr(Result,true)+')');{$endif}
end;

end.

