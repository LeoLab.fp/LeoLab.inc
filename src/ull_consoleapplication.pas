unit ull_ConsoleApplication;

{$mode objfpc}{$H+}

interface

uses
  Classes, CustApp, SysUtils,IniFiles,fileinfo,
  ull_lfile,
  ull_mparse
//  ull_fileutils
  ;

type
  TLL_TerminateReason=(trFinish,trError,trInterrupt);

  TLL_ConsoleApplication=class(TCustomApplication)
  private
    cfg_logTimeFmt:string;
    cfg_logFileName:string;
    cfg_FileName:string;
    verbose:boolean;
    fLog:TLL_LFile;
    MParse:TLL_MParse;
  protected
    procedure DoRun(); override;
    function Init():boolean; virtual;
    procedure writeHelp(); virtual; abstract;
    procedure Process(); virtual; abstract;
    procedure doLog(et:TEventType;const msg:string); override;
  //== Logging >>
    procedure _dbg(const msg:string);
    procedure _msg(const msg:string);
    procedure _info(const msg:string);
    procedure _warn(const msg:string);
    procedure _err(const msg:string);
    procedure _crit(const msg:string);
    {$IFDEF dbg}procedure _dbg(const m,s:string);{$ENDIF}
  //<< Logging ==
    procedure writeVersion(const short:boolean);
    function getOpts():boolean;
    function getConf(const iniFile:string):boolean;
  public
    procedure Terminate(); override;
    procedure Terminate(const AReason:TLL_TerminateReason);
    constructor Create(AOwner: TComponent);override;
    destructor Destroy();override;
    function EventTypeToString(et:TEventType):string;
  end;

implementation

function TLL_ConsoleApplication.getOpts():boolean;
begin
  {$IFDEF dbg}Self._dbg('getOpts','Enter');{$ENDIF}
  Result:=true;
  //
  {$IFDEF dbg}Self._dbg('getOpts','Leave('+BoolToStr(Result)+')');{$ENDIF}
end;

function TLL_ConsoleApplication.getConf(const iniFile:string):boolean;
var
  ini:TIniFile;

begin
  {$IFDEF dbg}Self._dbg('getOpts','Enter');{$ENDIF}
  Result:=true;
  ini:=TIniFile.Create(iniFile);
  try
    //
  finally
    FreeAndNil(ini);
  end;
  {$IFDEF dbg}Self._dbg('getOpts','Leave('+BoolToStr(Result)+')');{$ENDIF}
end;

procedure TLL_ConsoleApplication.DoRun();
begin
  {$IFDEF dbg}Self._dbg('DuRun','Enter');{$ENDIF}
  try
    try
      if Self.Init() then Self.Process();
    except
      on E:Exception do
      begin
        Self._crit(E.ToString());
      end;
    end;
  finally
    Self.Terminate;
  end;
  {$IFDEF dbg}Self._dbg('DoRun','Leave');{$ENDIF}
end;

function TLL_ConsoleApplication.Init():boolean;
begin
  {$IFDEF dbg}Self._dbg('Init','Enter');{$ENDIF}
  Result:=true;
  if Self.HasOption('h','help') then begin Self.writeHelp(); exit(false);end;
  if Self.HasOption('v','version') then begin Self.writeVersion(false); exit(false);end;
  if Self.HasOption('ver') then begin Self.writeVersion(true); exit(false);end;

  if Self.HasOption('ini.file') then Self.cfg_FileName:=Self.GetOptionValue('ini.file');
  if Self.HasOption('log.file') then Self.cfg_logFileName:=Self.GetOptionValue('log.file');
  if Self.HasOption('verbose') then Self.verbose:=true;

  //== Обработка параметров запуска и конфигурации >>
  if not Self.getConf('/etc/'+ExtractFileName(Self.ExeName)+'.ini') then Result:=false;

  if not Self.getOpts() then Result:=false;
  //<< Обработка параметров запуска и конфигурации ==
  {$IFDEF dbg}Self._dbg('Init','Leave('+BoolToStr(Result)+')');{$ENDIF}
end;

procedure TLL_ConsoleApplication.writeVersion(const short:boolean);
var
  FileInfoVer:TFileVersionInfo;
begin
  FileInfoVer:=TFileVersionInfo.Create(nil);
  try
    FileInfoVer.ReadFileInfo();
    if short then writeln(FileInfoVer.VersionStrings.Values['FileVersion'])
    else begin
      writeln('Company: '+FileInfoVer.VersionStrings.Values['CompanyName']);
      writeln('File version: '+FileInfoVer.VersionStrings.Values['FileVersion']);
      writeln('Product name: '+FileInfoVer.VersionStrings.Values['ProductName']);
      writeln('Product version: '+FileInfoVer.VersionStrings.Values['ProductVersion']);
    end;
  finally
    FreeAndNil(FileInfoVer);
  end;
end;

//==== Логгирование >>>>

procedure TLL_ConsoleApplication._dbg(const msg:string);
begin
  Self.Log(etDebug,msg);
end;

procedure TLL_ConsoleApplication._msg(const msg:string);
begin
  Self.Log(etCustom,msg);
end;

procedure TLL_ConsoleApplication._info(const msg:string);
begin
  Self.Log(etInfo,msg);
end;

procedure TLL_ConsoleApplication._warn(const msg:string);
begin
  Self.Log(etWarning,msg);
end;

procedure TLL_ConsoleApplication._err(const msg:string);
begin
  Self.Log(etError,msg);
end;

procedure TLL_ConsoleApplication._crit(const msg:string);
begin
  Self.Log(etError,msg);
  Self.Terminate(trError);
end;

{$IFDEF dbg}
procedure TLL_ConsoleApplication._dbg(const m,s:string);
begin
  writeln('[DBG] '+Self.ClassName+'.'+m+': '+s);
end;
{$ENDIF}

function TLL_ConsoleApplication.EventTypeToString(et:TEventType):string;
begin
  case et of
    etError:Result:='ERROR';
    etWarning:Result:='WARN';
    etInfo:Result:='INFO';
    etCustom:Result:='MSG';
    etDebug:Result:='DBG';
    else Result:='Unknown';
  end;
end;

procedure TLL_ConsoleApplication.doLog(et:TEventType;const msg:string);
begin
  if assigned(Self.fLog) then Self.fLog.WriteLine('['+Self.EventTypeToString(et)+']'#9+msg);
  if Self.verbose then writeln('['+Self.EventTypeToString(et)+'] '#9+msg);
end;

//<<<< Логгирование ====

constructor TLL_ConsoleApplication.Create(AOwner: TComponent);
begin
  {$IFDEF dbg}Self._dbg('Create','Enter');{$ENDIF}
  inherited Create(AOwner);
  Self.cfg_logFileName:='';//ExtractFileName(Self.ExeName)+'.log';
  Self.cfg_logTimeFmt:='hh:nn:ss.zzz';
  Self.verbose:=false;
  {$IFDEF dbg}Self._dbg('Create','Leave');{$ENDIF}
end;

destructor TLL_ConsoleApplication.Destroy();
begin
  {$IFDEF dbg}Self._dbg('Destroy','Enter');{$ENDIF}
  if Assigned(Self.fLog) then FreeAndNil(Self.fLog);
  if Assigned(Self.MParse) then FreeAndNil(Self.MParse);
  {$IFDEF dbg}Self._dbg('Destroy','Leave');{$ENDIF}
  inherited Destroy();
end;

procedure TLL_ConsoleApplication.Terminate();
begin
  Self.Terminate(trFinish);
end;

procedure TLL_ConsoleApplication.Terminate(const AReason:TLL_TerminateReason);
begin
  //@todo: Обработка причины завершения
  case AReason of
    trFinish: Self._msg('Finishing');
    trInterrupt: Self._warn('App finishing by interrupt.');
    trError: Self._err('App finishing by ERROR');
  end;
  inherited Terminate;
end;

{$IFDEF dbg}
initialization
  writeln('Starting in DEBUGE mode');
finalization
  writeln('DEBUGE mode finished');
{$ENDIF}
end.

