unit ull_MParse;
(*
@descript:
  Хелпер для замены "макросов" в строках.
  Все макросы заключаются в парные символы "%" (процент)
  При добавлении макроса символы-ограничители подставляются автоматически.

@version:
@date:

@changes:
  #2017-10-11:ELeonov
    [+] Режим отладочной сборки

  #2016-10-14:ELeonov
    [*] Класс внесен в библиотеку.
    [+] Макрос: %time.Z% - микросекунды текущего времени
    [+] Метод: .Upd(k,v) - производит замену существующего макроса. При отсутствии добавляет новый.
    [+] Метод: .tfSet(k,v) - производит замену поля формата даты/времени
    [+] Произвольное форматирование даты/времени
        Возможные ключи форматирования даты/времени:
          date.Y
          date.M
          date.D
          date
          time.H
          time.M
          time.S
          time.Z
          time
        (!) Метод .Reset() сбрасывает произвольные настройки форматирования даты/времени

  #2016-09-30:ELeonov
    [+] Метод .Reset()
    [+] Обновление значений даты/времени при вызове Parse(string,true)

  #2016-08-22:ELeonov
    [f] Перекрытие метода .Destroy()

  #2016-07-18:ELeonov
*)
{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils;

type
  TLL_MParse=class(TObject)
  private
    SR:TStringList;
    tf:TStringList;
  protected
    {$IFDEF dbg}procedure _dbg(const f,m:string);{$ENDIF}
  public
    constructor Create();
    destructor Destroy(); override;
    function Parse(s:string;u:boolean=false):string;
    procedure Add(k,v:string);
    procedure Upd(k,v:string);
    procedure tfSet(k,v:string);
    procedure Reset();
  end;

implementation

function TLL_MParse.Parse(s:string;u:boolean=false):string;
var
  i:Integer;
  k,v:string;
  dt:TDateTime;
begin
  {$IFDEF dbg}Self._dbg('Parse','Enter('+s+')');{$ENDIF}
  if u then
  begin
    {$IFDEF dbg}Self._dbg('Parse','Update datetime');{$ENDIF}
    dt:=Now();
    Self.SR.Values['%date.Y%']:=FormatDateTime(Self.tf.Values['date.Y'],dt);
    Self.SR.Values['%date.M%']:=FormatDateTime(Self.tf.Values['date.M'],dt);
    Self.SR.Values['%date.D%']:=FormatDateTime(Self.tf.Values['date.D'],dt);
    Self.SR.Values['%date%']:=FormatDateTime(Self.tf.Values['date'],dt);
    Self.SR.Values['%time.H%']:=FormatDateTime(Self.tf.Values['time.H'],dt);
    Self.SR.Values['%time.M%']:=FormatDateTime(Self.tf.Values['time.M'],dt);
    Self.SR.Values['%time.S%']:=FormatDateTime(Self.tf.Values['time.S'],dt);
    Self.SR.Values['%time.Z%']:=FormatDateTime(Self.tf.Values['time.Z'],dt);
    Self.SR.Values['%time%']:=FormatDateTime(Self.tf.Values['time'],dt);
    {$IFDEF dbg}Self._dbg('Parse','Datetime updated');{$ENDIF}
  end;
  Result:=s;
  for i:=0 to Self.SR.Count-1 do
  begin
    Self.SR.GetNameValue(i,k,v);
    Result:=StringReplace(Result,k,v,[rfReplaceAll,rfIgnoreCase]);
  end;
  {$IFDEF dbg}Self._dbg('Parse','Leave('+Result+')');{$ENDIF}
end;

procedure TLL_MParse.tfSet(k,v:string);
begin
  {$IFDEF dbg}Self._dbg('tfSet','('+k+'='+v+')');{$ENDIF}
  Self.tf.Values[k]:=v;
end;

procedure TLL_MParse.Add(k,v:string);
begin
  {$IFDEF dbg}Self._dbg('Add','('+k+'='+v+')');{$ENDIF}
  Self.SR.Add('%'+k+'%='+v);
end;

procedure TLL_MParse.Upd(k,v:string);
begin
  {$IFDEF dbg}Self._dbg('Upd','('+k+'='+v+')');{$ENDIF}
  Self.SR.Values['%'+k+'%']:=v;
end;

procedure TLL_MParse.Reset();
var
  dt:TDateTime;
begin
  {$IFDEF dbg}Self._dbg('Reset','Enter');{$ENDIF}
  dt:=Now();
  Self.tf.Clear();
  Self.tf.Values['date.Y']:='YYYY';
  Self.tf.Values['date.M']:='MM';
  Self.tf.Values['date.D']:='DD';
  Self.tf.Values['date']:='YYYY-MM-DD';
  Self.tf.Values['time.H']:='hh';
  Self.tf.Values['time.M']:='nn';
  Self.tf.Values['time.S']:='ss';
  Self.tf.Values['time.Z']:='zzz';
  Self.tf.Values['time']:='hh-nn-ss';

  Self.SR.Clear;
  Self.SR.Add('%date.Y%='+FormatDateTime('YYYY',dt));
  Self.SR.Add('%date.M%='+FormatDateTime('MM',dt));
  Self.SR.Add('%date.D%='+FormatDateTime('DD',dt));
  Self.SR.Add('%date%='+FormatDateTime('YYYY-MM-DD',dt));
  Self.SR.Add('%time.H%='+FormatDateTime('hh',dt));
  Self.SR.Add('%time.M%='+FormatDateTime('nn',dt));
  Self.SR.Add('%time.S%='+FormatDateTime('ss',dt));
  Self.SR.Add('%time%='+FormatDateTime('hh-nn-ss',dt));
  {$IFDEF dbg}Self._dbg('Reset','Leave');{$ENDIF}
end;

constructor TLL_MParse.Create();
begin
  inherited Create();
  {$IFDEF dbg}Self._dbg('Create','Enter');{$ENDIF}
  Self.SR:=TStringList.Create();
  Self.tf:=TStringList.Create();
  Self.Reset();
  {$IFDEF dbg}Self._dbg('Create','Leave');{$ENDIF}
end;

destructor TLL_MParse.Destroy();
begin
  {$IFDEF dbg}Self._dbg('Destroy','Enter');{$ENDIF}
  FreeAndNil(Self.SR);
  FreeAndNil(Self.tf);
  {$IFDEF dbg}Self._dbg('Destroy','Leave');{$ENDIF}
  inherited Destroy;
end;

{$IFDEF dbg}
procedure TLL_MParse._dbg(const f,m:string);
begin
  writeln('['+FormatDateTime('hh:nn:ss.zzz',Now())+'] '+Self.ClassName+'.'+f+': '+m);
end;
{$ENDIF}

end.

