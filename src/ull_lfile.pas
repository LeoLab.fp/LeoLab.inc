unit ull_lfile;
{ $DEFINE dbg}

(*
@descript: Класс для записи в файл с автоматическим изменением имени по шаблону

@usage:
```
var
  TF:TLL_LFile;

begin
  TF:=TLL_LFile.Create();
  TF.fileName:='%base.path%/log/%date%/%time.H%.%proc.name%.%proc.id%.log';
  TF.prefString:='[%time%] ';
  TF.pParse.tfSet('time','nn:ss.zzz');
  TF.WriteLine('Файл будет создан автоматически.');
  TF.Free();
end;
```

@version:
@date:

@changes:
  #2017-10-11:ELeonov
    [*] Изменение вывода отладочной информации

  #2016-10-14:ELeonov
    [i] Создание класса
    [+] Возможность задания произвольной строки-префикса для записываемых строк.
*)

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,
  ull_MParse,ull_fileUtils;

type
  TLL_LFile=class(TObject)
  private
    fMParse:TLL_MParse; //Парсер, используется для обработки имени файла
    fpParse:TLL_MParse; //Парсер, используется для обработки префикса записываемой строки
    fAppend:boolean; //Добавлять к существующему файлу
    fName:string; //Имя файла (базовое)
    fPref:string; //Префикс записываемой строки
    fFile:TFileStream;
    procedure checkFileName();
  protected
    procedure setMParse(p:TLL_MParse);
    {$IFDEF dbg}procedure _dbg(const f,m:string);{$ENDIF}
  public
    property fileName:string read fName write fName;
    property prefString:string read fPref write fPref;
    property MParse:TLL_MParse read fMParse write setMParse;
    property pParse:TLL_MParse read fpParse;
    procedure WriteLine(s:string);
    constructor Create();
    destructor Destroy();override;
  end;

implementation

procedure TLL_LFile.checkFileName();
var
  fn:string;
begin
  {$IFDEF dbg}Self._dbg('checkFileName','Enter('+Self.fileName+')');{$ENDIF}
  if Self.fileName='' then exit;
  if Assigned(Self.MParse) then
  begin
    {$IFDEF dbg}Self._dbg('checkFileName','[call] MParse.Parse('+Self.fileName+')');{$ENDIF}
    fn:=Self.MParse.Parse(Self.fileName,true);
    {$IFDEF dbg}Self._dbg('checkFileName','[call return] '+fn);{$ENDIF}
  end else fn:=Self.fileName;
  if Self.fFile<>nil then if Self.fFile.FileName<>fn then
  begin
    {$IFDEF dbg}Self._dbg('checkFileName','File name changed, destroy FileStream instance.');{$ENDIF}
    FreeAndNil(Self.fFile);
  end;
  if Self.fFile=nil then
  begin
    {$IFDEF dbg}Self._dbg('checkFileName','Create new FileStream instance');{$ENDIF}
    if FileExists(fn) then
    begin
      if Self.fAppend then
      begin
        {$IFDEF dbg}Self._dbg('checkFileName','Append mode');{$ENDIF}
        Self.fFile:=TFileStream.Create(fn,fmOpenWrite);
      end else
      begin
        {$IFDEF dbg}Self._dbg('checkFileName','Replace mode');{$ENDIF}
        Self.fFile:=TFileStream.Create(fn,fmCreate);
      end;
    end else
    begin
      {$IFDEF dbg}Self._dbg('checkFileName','[call] ll_MLPath('+fn+')');{$ENDIF}
      ll_MKPath(ExtractFileDir(fn));
      {$IFDEF dbg}Self._dbg('checkFileName','New file: '+fn);{$ENDIF}
      Self.fFile:=TFileStream.Create(fn,fmCreate);
    end;
    {$IFDEF dbg}Self._dbg('checkFileName','New FileStream instance created');{$ENDIF}
  end;
  {$IFDEF dbg}Self._dbg('checkFileName','Leave('+fn+')');{$ENDIF}
end;

procedure TLL_LFile.WriteLine(s:string);
var
  ws:string;
begin
  {$IFDEF dbg}Self._dbg('WriteLine','Enter('+s+')');{$ENDIF}
  Self.checkFileName();
  if Self.fFile<>nil then
  begin
    if Self.fPref<>'' then ws:=Self.fpParse.Parse(Self.fPref,true)+' ' else ws:='';
    ws:=ws+s+LineEnding;
    Self.fFile.Write(Pointer(ws)^,Length(ws));
  end;
  {$IFDEF dbg}Self._dbg('WriteLine','Leave');{$ENDIF}
end;

procedure TLL_LFile.setMParse(p:TLL_MParse);
begin
  {$IFDEF dbg}Self._dbg('setMParse','Assign MParse');{$ENDIF}
  Self.fMParse:=p;
end;

constructor TLL_LFile.Create();
begin
  inherited Create();
  {$IFDEF dbg}Self._dbg('Create','Enter');{$ENDIF}
  Self.fpParse:=TLL_MParse.Create();
  Self.fpParse.tfSet('date','YYYY-MM-DD');
  Self.fpParse.tfSet('time','hh:nn:ss.zzz');
  Self.fAppend:=false;
  //
  {$IFDEF dbg}Self._dbg('Create','Leave');{$ENDIF}
end;

destructor TLL_LFile.Destroy();
begin
{$IFDEF dbg}Self._dbg('Destroy','Enter');{$ENDIF}
  Self.fpParse.Free();
  if Assigned(Self.fFile) then Self.fFile.Free();
  {$IFDEF dbg}Self._dbg('Destroy','Leave');{$ENDIF}
  inherited Destroy();
end;

{$IFDEF dbg}
procedure TLL_LFile._dbg(const f,m:string);
begin
  writeln('['+FormatDateTime('hh:nn:ss.zzz',Now())+'] '+Self.ClassName+'.'+f+': '+m);
end;
{$ENDIF}

end.

