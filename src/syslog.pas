unit syslog;

{$mode objfpc}{$H+}

{
  Режим отладки включается ТОЛЬКО если при компиляции с ключем -ddbg
  (Определена переменная условной компиляции "dbg")
}

interface

uses
  SysUtils,systemlog;

type

  TLogProc=procedure(et:TEventType;const msg:string)of object;

const
  __eType2str:array[0..4]of string=(
    'MSG', //etCustom
    'INFO', //etInfo
    'WARN', //etWarning
    'ERROR', //etError
    'DBG'  //etDebug
  );

  __eType2log:array[0..4]of dword=(
    LOG_NOTICE, //etCustom
    LOG_INFO, //etInfo
    LOG_WARNING, //etWarning
    LOG_ERR, //etError
    LOG_DEBUG  //etDebug
  );

  __LLevel2ET:array[0..4]of TEventTypes=(
    [etError],
    [etError,etWarning],
    [etError,etWarning,etInfo],
    [etError,etWarning,etInfo,etCustom],
    [etError,etWarning,etInfo,etCustom,etDebug]
  );

//procedure log(const __pri:longint;const msg:string);
procedure elog(et:TEventType;const msg:string);
{$ifdef dbg}
procedure dbgout(const f,m:string);
{$endif}
function EventTypeToString(et:TEventType):string;
function logLevelToEventTypes(aLevel:integer):TEventTypes;
function logLevelToEventTypes(aLevel:string):TEventTypes;

(*
type
  dbglog=object
    procedure enter(const f:string);
    procedure leave(const f:string);
    procedure note(const f,m:string);
  end;
*)

var
  StdDbg:Text;

implementation

var
  dbg_file:boolean=false;
  dbg_verbose:boolean=false;
  dbg_tsFormat:string='hh:nn:ss.zzz';


procedure log(const __pri:longint;const msg:string);
begin
  systemlog.syslog(__pri,PChar(msg),[]);
end;

{$ifdef dbg}
procedure dbgout(const f,m:string);
var
//  FD:Text;
//  fn,
    s:string;
begin
  if dbg_file or dbg_verbose then s:=FormatDateTime(dbg_tsFormat,Now())+' '+f+' '+m;
  if dbg_file then begin
    writeln(StdDbg,s);
    Flush(StdDbg);
  end;
  if dbg_verbose then writeln(STDERR,'[DBG] '+s);
end;
{$endif}

function logLevelToEventTypes(aLevel: string): TEventTypes;
begin
  Result:=logLevelToEventTypes(StrToInt(aLevel));
end;

function logLevelToEventTypes(aLevel:integer):TEventTypes;
begin
  Result:=__LLevel2ET[aLevel];
end;

function EventTypeToString(et:TEventType):string;
begin
  Result:=__eType2str[ord(et)];
end;

procedure elog(et:TEventType;const msg:string);
begin
//  {$ifdef dbg}dbgout('log('+EventTypeToString(et)+')',msg);{$endif}
  log(__eType2log[ord(et)],msg);
end;

function __hasSingleOption(const key:string):boolean;
var
  c:integer;
begin
  Result:=false;
  for c:=1 to ParamCount() do if (ParamStr(c)='--'+key) or (ParamStr(c)='-'+key) then exit(true);
end;

{$ifdef dbg}
var
  fn:string;
{$endif}

initialization
{$IFDEF dbg}
//= Определяем наличие ключей --dbg.file или --dbg.verbose в параметрах запуска >
  dbg_file:=__hasSingleOption('dbg.file');
  dbg_verbose:=__hasSingleOption('dbg.verbose');
  if dbg_verbose then writeln(STDERR,'DBG.VERBOSE mode ON');// else writeln(STDERR,'DBG.VERBOSE mode OFF');
//< Определяем наличие ключей --dbg.file или --dbg.verbose в параметрах запуска =
  if dbg_file then
  begin
    fn:=GetCurrentDir()+'/'+ExtractFileName(paramstr(0))+'.'+IntToStr(GetProcessID())+'.dbglog';
    AssignFile(StdDbg,fn);
    if FileExists(fn) then Append(StdDbg) else Rewrite(StdDbg);
    if dbg_verbose then writeln(STDERR,'DBG_FILE mode ON, file: '+fn);
    dbgout('LeoLab.inc:syslog','Started');
  end;
{$ENDIF}
finalization
  closelog();
{$IFDEF dbg}
  if dbg_file then
  begin
    dbgout('LeoLab.inc:syslog','Finished');
    CloseFile(StdDbg);
  end;
{$ENDIF}
end.

