unit ull_strutils;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils;

type
  TStringArray = array of string;

function Explode(Ch:Char; const Text:string):TStringArray;
function Implode(Ch:Char; const AStr:TStringArray; const skipEmpty:boolean=false):string;

implementation

function Implode(Ch:Char; const AStr:TStringArray; const skipEmpty:boolean=false):string;
var
  c:integer;
begin
  Result:=AStr[0];
  for c:=1 to Length(AStr)-1 do
  begin
    if skipEmpty and (AStr[c]='') then continue;
    Result:=Result+Ch+AStr[c];
  end;
end;

function Explode(Ch: Char; const Text: string): TStringArray;
var
  i, k, Len: Integer;
  Count: Integer;
begin
  if Text = '' then
  begin
    Result := nil;
    Exit;
  end; // if
  Count := 0;
  Len := Length(Text);
  for i := 1 to Len do
  begin
    if Text[i] = Ch then Inc(Count);
  end; // for i
  SetLength(Result, Count + 1);
  Count := 0;
  k := 1;
  for i := 1 to Len do
  begin
    if Text[i] = Ch then
    begin
      Result[Count] := Copy(Text, k, i - k);
      Inc(Count);
      k := i + 1;
    end; // if
  end;
  Result[Count] := Copy(Text, k, Len - k + 1);
end;

end.

