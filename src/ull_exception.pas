unit ull_exception;

{$mode objfpc}{$H+}

interface

uses
  SysUtils;

type

  ELL_Exception=class(Exception); //Базовый класс ошибок библиотеки
                                  //Используется для определения происхождения ошибки

  ELL_NotImplemented=class(ELL_Exception); //Исключение "Не реализовано"


implementation

end.

