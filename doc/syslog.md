# syslog

Модуль логгирования

## Процедуры и функции

#### log
СКРЫТО

```pascal
procedure syslog.log(const __pri:longint; const msg:string);  
```

Производит запись в системный лог.  
Возможные значения параметра **__pri**:
  LOG_NOTICE = 5
  LOG_INFO = 6
  LOG_WARNING = 4
  LOG_ERR = 3
  LOG_DEBUG = 7

#### elog
```pascal
procedure elog(et:TEventType;const msg:string);
```

Производит запись в системный лог.  
Возможные значения параметра **et**:
  etCustom = 0 > LOG_NOTICE
  etInfo = 1 > LOG_INFO
  etWarning = 2 > LOG_WARNING
  etError = 3 > LOG_ERR
  etDebug = 4 > LOG_DEBUG

#### dbgout
```pascal
procedure dbgout(const f,m:string);
```

Производит запись в файл и вывод в STDERR отладочных сообщений.

Файл отладочных сообщений создается в ПАПКЕ ЗАПУСКА (текущей папке) с именем выполняемого файла, идентификатором процесса, идентификатором потока и расширением **.dbglog**

Файл отладочных сообщений создается при наличии ключа --dbg в аргументах запуска приложения.

Вывод в STDERR производится при наличии опции --dbg.verbose в аргументах запуска приложения.

Наличие ключей определяется при старте приложения.

## Переменные

## Константы
