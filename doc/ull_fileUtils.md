# ull_fileUtils

Модуль работы с файлами

## Процедуры и функции

### RealPath
```Pascal
function RealPath(const APath:string,const exePath:boolean=false):string;
```
Разворачивает указанный путь в абсолютный, в зависимости от значения параметра exePath:
  - true - относительно папки с исполняемым файлом
  - false - относительно текущей папки

### ll_FindExe
```Pascal
function ll_FindExe(const AFile:string; const userPath:boolean=true):string;
```
Производит поиск указанного выполняемого файла.  
Параметр usePath указывает на необходимость поиска в путях задаваемых переменной окружения PATH.

### ll_MKPath
```Pascal
function ll_MKPath(const APath:string):boolean;
```
Создает папку, и при необходимости путь к ней.

### ll_RMPath
```Pascal
function ll_RMPath(const APath:string):boolean;
```
Удаляет папку со всем содержимым.

## Переменные

## Константы
